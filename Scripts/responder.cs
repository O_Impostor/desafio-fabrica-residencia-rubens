using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class responder : MonoBehaviour
{
    private int idTema;

    public Text pergunta;
    public Text respostaA;
    public Text respostaB;
    public Text respostaC;
    public Text respostaD;
    public Text infoRespostas;

    public string[] perguntas;
    public string[] alternativaA;
    public string[] alternativaB;
    public string[] alternativaC;
    public string[] alternativaD;
    public string[] corretas;

    private int idPergunta;

    private float acertos;
    private float questoes;
    private float media;
    private int Nota;




    // Start is called before the first frame update
    void Start()
    {
        idTema = PlayerPrefs.GetInt("idTema");

        idPergunta = 0;
        questoes = perguntas.Length;
        pergunta.text = perguntas[idPergunta];
        respostaA.text = alternativaA[idPergunta];
        respostaB.text = alternativaB[idPergunta];
        respostaC.text = alternativaC[idPergunta];
        respostaD.text = alternativaD[idPergunta];

        infoRespostas.text = "Respondendo "+(idPergunta+1).ToString()+" de " + questoes.ToString() + " perguntas.";


    }

    public void resposta(string alternativa)
    {

        if (alternativa == "A")
        {
            if(alternativaA[idPergunta] == corretas[idPergunta])
            {
                acertos += 1;
            }
        }
        else if (alternativa == "B")
        {
            if (alternativaB[idPergunta] == corretas[idPergunta])
            {
                acertos += 1;
            }
        }
        else if (alternativa == "C")
        {
            if (alternativaC[idPergunta] == corretas[idPergunta])
            {
                acertos += 1;
            }
        }
        else if (alternativa == "D")
        {
            if (alternativaD[idPergunta] == corretas[idPergunta])
            {
                acertos += 1;
            }
        }
        proximaPergunta();

    }
    
    void proximaPergunta()
    {
        
        idPergunta += 1;

        if(idPergunta <= (questoes - 1))
        {

            pergunta.text = perguntas[idPergunta];
            respostaA.text = alternativaA[idPergunta];
            respostaB.text = alternativaB[idPergunta];
            respostaC.text = alternativaC[idPergunta];
            respostaD.text = alternativaD[idPergunta];

            infoRespostas.text = "Respondendo " + (idPergunta + 1).ToString() + " de " + questoes.ToString() + " perguntas.";

        }

        else
        {

            media = 10 * (acertos / questoes);
            Nota = Mathf.RoundToInt(media);

            if(Nota > PlayerPrefs.GetInt("Nota" + idTema.ToString()))
            {
                PlayerPrefs.SetInt("Nota"+idTema.ToString(), Nota);
                PlayerPrefs.SetInt("acertos" + idTema.ToString(), (int) acertos);
            }
            
            
            PlayerPrefs.SetInt("NotaTemp" + idTema.ToString(), Nota);
            PlayerPrefs.SetInt("acertosTemp" + idTema.ToString(), (int)acertos);

            Application.LoadLevel("Nota");

        }
        
    }

}
