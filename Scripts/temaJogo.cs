using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class temaJogo : MonoBehaviour
{

    public Button btnPlay;
    public Text txtNomeTema;

    public GameObject infoTema;
    public Text txtInfoTema;
    public GameObject estrela1;
    public GameObject estrela2;
    public GameObject estrela3;

    public string[] nomeTema;
    public int numeroQuestoes;

    private int idTema;



    void Start()
    {

        idTema = 0;
        txtNomeTema.text = nomeTema[idTema];
        txtInfoTema.text = "";
        infoTema.SetActive(false);
        estrela1.SetActive(false);
        estrela2.SetActive(false);
        estrela3.SetActive(false);
        btnPlay.interactable = false;

    }

    public void selecioneTema(int i)
    {

        idTema = i;
        PlayerPrefs.SetInt("idTema", idTema);
        txtNomeTema.text = nomeTema[idTema];

            int nota = PlayerPrefs.GetInt("NotaTemp" + idTema.ToString());
            int acertos = PlayerPrefs.GetInt("acertos" + idTema.ToString());
            estrela1.SetActive(false);
            estrela2.SetActive(false);
            estrela3.SetActive(false);

            if (nota == 10)
            {
                estrela1.SetActive(true);
                estrela2.SetActive(true);
                estrela3.SetActive(true);
            }
            else if (nota >= 7)
            {
                estrela1.SetActive(true);
                estrela2.SetActive(true);
                estrela3.SetActive(false);
            }
            else if (nota >= 5)
            {
                estrela1.SetActive(true);
                estrela2.SetActive(false);
                estrela3.SetActive(false);
            }

        txtInfoTema.text = "Voc� acertou "+acertos.ToString()+" de "+numeroQuestoes.ToString()+" quest�es!";
        infoTema.SetActive(true);
        btnPlay.interactable = true;

    }

    public void jogar()
    {

        Application.LoadLevel("T"+idTema.ToString());

    }


}
